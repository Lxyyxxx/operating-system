#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
using namespace std;

// 缓冲区个数、生产者线程数量、消费者线程数量
const int N = 16;
const int pN = 25;
const int cN = 20;
// const int pN = 20;
// const int cN = 25;

class Semaphore {
   public:
    // 信号量，因为本实验的信号量值只有0和1，所以采用互斥形式
    bool sem;
    // 初始化
    void init(bool val) { sem = val; }
    // 等待
    void wait() {
        while (sem == 0)
            ;
        sem = 0;
    }
    // 发出信号
    void signal() { sem = 1; }
};

// 信号量，一个缓冲区设置一个
Semaphore mutex[N], full[N], empty_[N];
// 缓冲区
int buffer[N];
// 消费者和生产者线程
pthread_t producers[pN], consumers[cN];
// 当前缓冲区分配
int user_c[N];

// 传递多个参数
struct multi {
    int id, buf_id;
};

// 生产者生产
void Producer(multi p_cur, int good) {
    printf("生产者%d生产了%d号产品，存放在%d号缓冲区\n", p_cur.id, good,
           p_cur.buf_id);
    // 放入缓冲区
    buffer[p_cur.buf_id] = good;
    printf("当前缓冲区：");
    for (int i = 0; i < N; i++)
        if (buffer[i] != -1) printf("%d:%d\t", i, buffer[i]);
    printf("\n");
}

// 消费者消费
void Consumer(multi c_cur) {
    printf("消费者%d从%d号缓冲区，购买了%d号产品\n", c_cur.id, c_cur.buf_id,
           buffer[c_cur.buf_id]);
    // -1 表示已取出
    buffer[c_cur.buf_id] = -1;
    printf("当前缓冲区：");
    for (int i = 0; i < N; i++)
        if (buffer[i] != -1) printf("%d:%d\t", i, buffer[i]);
    printf("\n");
}

// 喂给线程的生产者生产
void *runProducer(void *argv) {
    multi p_cur = *(multi *)argv;
    printf("生产者%d开始生产\n", p_cur.id);
    // empty能放
    empty_[p_cur.buf_id].wait();
    // 消费者没有操作缓冲区
    mutex[p_cur.buf_id].wait();
    // 生产
    Producer(p_cur, rand());
    // 释放缓冲区的锁
    mutex[p_cur.buf_id].signal();
    // 满了就给消费者发信号
    full[p_cur.buf_id].signal();
    // 结束线程
    pthread_join(producers[p_cur.id], NULL);
    printf("生产者%d生产完毕\n", p_cur.id);
}

// 喂给线程的消费者消费
void *runConsumer(void *argv) {
    multi c_cur = *(multi *)argv;
    // 等待可以从缓冲区取货
    while (user_c[c_cur.buf_id] != c_cur.id)
        ;
    printf("消费者%d开始消费\n", c_cur.id);
    // full能取
    full[c_cur.buf_id].wait();
    // 生产者没有操作缓冲区
    mutex[c_cur.buf_id].wait();
    // 消费
    Consumer(c_cur);
    // 释放缓冲区的锁
    mutex[c_cur.buf_id].signal();
    // 空了就给消费者发信号
    empty_[c_cur.buf_id].signal();
    // 结束线程
    pthread_join(consumers[c_cur.id], NULL);
    printf("消费者%d消费完毕\n", c_cur.id);
    // 分配给下一个消费者
    user_c[c_cur.buf_id] = c_cur.id + N;
}

int main() {
    printf("生产者个数%d，消费者个数%d\n", pN, cN);
    // 初始化
    for (int i = 0; i < N; i++) {
        mutex[i].init(1);
        full[i].init(0);
        empty_[i].init(1);
        buffer[i] = -1;
        user_c[i] = i;
    }
    // 随机数种子，用于生成产品
    srand((unsigned)time(NULL));
    // 开始模拟，按顺序去缓冲区放/取产品
    multi c_cur[cN], p_cur[pN];
    // 消费者开始消费
    for (int i = 0; i < cN; i++) {
        c_cur[i] = {i, i % N};
        pthread_create(&consumers[i], NULL, runConsumer, (multi *)&(c_cur[i]));
    }
    // 生产者开始生产
    for (int i = 0; i < pN; i++) {
        p_cur[i] = {i, i % N};
        pthread_create(&producers[i], NULL, runProducer, (multi *)&(p_cur[i]));
    }
    // 消费者比生产者多时，生产者进行多次生产，避免死锁
    for (int i = 0; i < cN - pN; i++) {
        p_cur[i] = {i, (pN + i) % N};
        pthread_create(&producers[i], NULL, runProducer, (multi *)&(p_cur[i]));
    }
    // 等待所有线程结束
    sleep(1);
    return 0;
}