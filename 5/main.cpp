#include <bits/stdc++.h>
using namespace std;

const int MaxNumber = 100;
// 页面顺序
int PageOrder[MaxNumber];
// 物理块
int PageCount[MaxNumber];
// 页面总数，缺页数
int PageNum, LackNum;
// 缺页率
double LackPageRate;
// 是否命中
bool found;

// 是否命中页面
bool findPage(int page, int m) {
    for (int i = 0; i < m; i++) {
        if (PageCount[i] == page) return true;
    }
    return false;
}

// 打印当前页面的分配
void print(int m) {
    for (int i = 0; i < m; i++) {
        if (i != 0) cout << " ";
        cout << PageCount[i];
    }
    cout << '\n';
}

// 先进先出
void FIFO(int m) {
    cout << "FIFO开始工作\n";
    LackNum = 0;
    // 指向当前被替换的页面
    int cur = 0;
    // 当前总共装了几个页面
    int cnt = 0;
    for (int i = 0; i < PageNum; i++) {
        // 物理块未装满
        if (cnt < m) {
            PageCount[cnt++] = PageOrder[i];
            LackNum++;
            print(cnt);
        } else {
            found = findPage(PageOrder[i], m);
            // 命中
            if (found) {
                cout << "页面" << PageOrder[i] << "命中\n";
            } else {
                LackNum++;
                // 替换
                PageCount[cur++] = PageOrder[i];
                if (cur >= m) cur %= m;
                // 打印
                print(m);
            }
        }
    }
    LackPageRate = LackNum * 1.0 / PageNum;
    cout << "缺页次数：" << LackNum << '\n';
    printf("缺页率：%.02lf%%\n", LackPageRate * 100);
    cout << "FIFO工作结束\n";
}

// 最佳置换
void OPI(int m) {
    cout << "OPI开始工作\n";
    LackNum = 0;
    // 指向当前被替换的页面
    int cur = 0;
    // 当前总共装了几个页面
    int cnt = 0;
    for (int i = 0; i < PageNum; i++) {
        // 物理块未装满
        if (cnt < m) {
            PageCount[cnt++] = PageOrder[i];
            LackNum++;
            print(cnt);
        } else {
            found = findPage(PageOrder[i], m);
            // 命中
            if (found) {
                cout << "页面" << PageOrder[i] << "命中\n";
            } else {
                LackNum++;
                // 寻找未来时间最久没使用的
                bool vis[m] = {false};
                int cntFound = 0;
                for (int j = i + 1; j < PageNum; j++) {
                    for (int k = 0; k < m; k++) {
                        // 找到且未重复找到
                        if (!vis[k] && PageCount[k] == PageOrder[j]) {
                            vis[k] = true;
                            cntFound++;
                            break;
                        }
                    }
                    // 剩下一个就是要淘汰的，或者到了最后一个页面还有多个可淘汰的
                    if ((cntFound == m - 1) || (PageNum - 1 == j)) {
                        for (int k = 0; k < m; k++) {
                            if (!vis[k]) {
                                cur = k;
                                break;
                            }
                        }
                    }
                }
                // 替换
                PageCount[cur] = PageOrder[i];
                // 打印
                print(m);
            }
        }
    }
    LackPageRate = LackNum * 1.0 / PageNum;
    cout << "缺页次数：" << LackNum << '\n';
    printf("缺页率：%.02lf%%\n", LackPageRate * 100);
    cout << "OPI工作结束\n";
}

// 最近最久未使用
void LRU(int m) {
    cout << "LRU开始工作\n";
    LackNum = 0;
    // 指向当前被替换的页面
    int cur = 0;
    // 当前总共装了几个页面
    int cnt = 0;
    // 模拟栈
    vector<int> stk;
    for (int i = 0; i < PageNum; i++) {
        // 物理块未装满
        if (cnt < m) {
            PageCount[cnt++] = PageOrder[i];
            stk.push_back(PageOrder[i]);
            LackNum++;
            print(cnt);
        } else {
            found = findPage(PageOrder[i], m);
            // 命中
            if (found) {
                cout << "页面" << PageOrder[i] << "命中\n";
                // 出栈再入栈
                stk.erase(find(stk.begin(), stk.end(), PageOrder[i]));
                stk.push_back(PageOrder[i]);
            } else {
                LackNum++;
                // 寻找之前最久没使用的
                int change = stk[0];
                stk.erase(stk.begin());
                for (int k = 0; k < m; k++) {
                    if (PageCount[k] == change) {
                        cur = k;
                        break;
                    }
                }
                // 替换
                PageCount[cur] = PageOrder[i];
                stk.push_back(PageOrder[i]);
                // 打印
                print(m);
            }
        }
    }
    LackPageRate = LackNum * 1.0 / PageNum;
    cout << "缺页次数：" << LackNum << '\n';
    printf("缺页率：%.02lf%%\n", LackPageRate * 100);
    cout << "LRU工作结束\n";
}

int main() {
    int m, n, choose;
    cout << "最小物理块数：";
    cin >> m;
    cout << "页面个数：";
    cin >> n;
    PageNum = n;
    cout << "页面序列：";
    for (int i = 0; i < n; i++) cin >> PageOrder[i];
    cout << "选择算法(1-FIFO，2-OPI，3-LRU)：";
    cin >> choose;
    if (choose == 1) {
        FIFO(m);
    } else if (choose == 2) {
        OPI(m);
    } else if (choose == 3) {
        LRU(m);
    }
    return 0;
}

/*
3
20
7 0 1 2 0 3 0 4 2 3 0 3 2 1 2 0 1 7 0 1
*/