#include <bits/stdc++.h>
using namespace std;

// 总进程数
const int N = 100;

// 到达时间
int ArrivalTime[N];
// 服务时间
int ServiceTime[N];
// 剩余服务时间
int PServiceTime[N];
// 完成时间
int FinishTime[N];
// 周转时间
int WholeTime[N];
// 带权周转时间
double WeightWholeTime[N];
// 周转时间的平均值
double AverageWT_HRRN, AverageWT_RR;
// 带权周转时间的平均值
double AverageWWT_HRRN, AverageWWT_RR;
// 是否结束
bool Finished[N];

// 高响应比优先
// n-进程个数
void HRRN(int n) {
    cout << "HRRN开始调度\n";
    // 当前时刻，表示哪个进程开始运行了
    // 开始时间为第一个到达时间
    int current = ArrivalTime[0];
    // 响应比
    double ResponseRatio[N] = {0};
    // 进程编号，最高响应比，最高响应比作业即选择执行的进程
    int cur, high, high_cur;
    // 当前未执行的进程个数
    int wait = n;
    // 每执行一个进程都做一次操作
    while (wait--) {
        high = 0;
        for (int i = 0; i < n; i++) {
            // 响应比=（等待+服务）/服务
            // 等待=当前-到达
            int waitTime = current - ArrivalTime[i];
            if (waitTime >= 0 && !Finished[i]) {
                // 计算响应比
                ResponseRatio[i] = (waitTime + ServiceTime[i]) / ServiceTime[i];
                if (ResponseRatio[i] > high) {
                    high = ResponseRatio[i];
                    high_cur = i;
                }
            }
        }
        // 执行它
        Finished[high_cur] = true;
        cout << "时刻" << current << "：进程" << char(high_cur + 'A')
             << "开始运行\n";
        // 完成=开始服务+服务
        FinishTime[high_cur] = current + ServiceTime[high_cur];
        // 周转=完成-到达
        WholeTime[high_cur] = FinishTime[high_cur] - ArrivalTime[high_cur];
        // 带权周转=周转/服务
        WeightWholeTime[high_cur] =
            WholeTime[high_cur] * 1.0 / ServiceTime[high_cur];
        cout << "时刻" << FinishTime[high_cur] << "：进程"
             << char(high_cur + 'A') << "运行完成";
        cout << "\n周转时间：" << WholeTime[high_cur];
        cout << "\n带权周转时间：" << WeightWholeTime[high_cur] << '\n';
        // 当前时刻设为已完成时间，作为下一个进程的开始执行时间
        current = FinishTime[high_cur];
    }
    cout << "时刻" << current << "：所有进程运行完毕\n";
    // 计算周转时间的平均值和带权周转时间的平均值
    double Sum_WT_HRRN = 0, Sum_WWT_HRRN = 0;
    for (int i = 0; i < n; i++) {
        Sum_WT_HRRN += WholeTime[i];
        Sum_WWT_HRRN += WeightWholeTime[i];
    }
    Sum_WT_HRRN /= n, Sum_WWT_HRRN /= n;
    cout << "平均周转时间：" << Sum_WT_HRRN << "\n平均带权周转时间："
         << Sum_WWT_HRRN;
    cout << "\nHRRN调度结束\n";
}

// 时间片轮转
// n-进程个数，q-时间片大小
void RR(int n, int q) {
    cout << "RR开始调度\n";
    // 就绪队列
    queue<int> que;
    // 当前时刻，表示哪个进程开始运行了
    // 开始时间为第一个到达时间
    int current = ArrivalTime[0];
    // 时刻0进入就绪队列
    int cur = 0;
    while (cur >= 0 && cur < n && ArrivalTime[cur] == current) que.push(cur++);
    while (!que.empty()) {
        int now = que.front();
        que.pop();
        cout << "时刻" << current << "：进程" << char(now + 'A')
             << "开始运行\n";
        int run = min(q, PServiceTime[now]);
        // q时间或剩余时间内其他到达进程入队
        while (cur < n && ArrivalTime[cur] <= current + run) que.push(cur++);
        // 能运行完
        if (PServiceTime[now] == run) {
            PServiceTime[now] -= run;
            Finished[now] = true;
            // 完成=当前+剩余服务时间
            FinishTime[now] = current + run;
            // 周转=完成-到达
            WholeTime[now] = FinishTime[now] - ArrivalTime[now];
            // 带权周转=周转/服务
            WeightWholeTime[now] = WholeTime[now] * 1.0 / ServiceTime[now];
            cout << "时刻" << FinishTime[now] << "：进程" << char(now + 'A')
                 << "运行完成";
            cout << "\n周转时间：" << WholeTime[now];
            cout << "\n带权周转时间：" << WeightWholeTime[now] << '\n';
            // 当前时刻设为已完成时间，作为下一个进程的开始执行时间
            current = FinishTime[now];
        } else {
            // 更新剩余时间
            PServiceTime[now] -= run;
            // 重新入队
            que.push(now);
            current += run;
        }
    }
    cout << "时刻" << current << "：所有进程运行完毕\n";
    // 计算周转时间的平均值和带权周转时间的平均值
    double Sum_WT_RR = 0, Sum_WWT_RR = 0;
    for (int i = 0; i < n; i++) {
        Sum_WT_RR += WholeTime[i];
        Sum_WWT_RR += WeightWholeTime[i];
    }
    Sum_WT_RR /= n, Sum_WWT_RR /= n;
    cout << "平均周转时间：" << Sum_WT_RR << "\n平均带权周转时间："
         << Sum_WWT_RR;
    cout << "\nRR调度结束\n";
}

int main() {
    // 进程个数
    cout << "请输入进程个数：";
    int n;
    cin >> n;
    if (n > N) {
        cout << n << "超过了最大限制，最大为" << N;
        return 0;
    }
    // 选择算法，1-HRRN，2-RR
    int choose;
    // 时间片大小
    int q;
    cout << "请输入到达时间：";
    // 输入到达时间
    // 默认是按时间顺序输入的，后面无排序操作
    for (int i = 0; i < n; i++) {
        cin >> ArrivalTime[i];
    }
    cout << "请输入服务时间：";
    // 输入服务时间
    for (int i = 0; i < n; i++) {
        cin >> ServiceTime[i];
        // 剩余服务时间初始化为总服务时间
        PServiceTime[i] = ServiceTime[i];
    }
    cout << "请选择算法，1-HRRN，2-RR\n";
    // 输入算法选择
    cin >> choose;
    if (choose == 1)
        HRRN(n);
    else if (choose == 2) {
        cout << "请输入时间片大小：";
        cin >> q;
        RR(n, q);
    } else
        cout << "请输入1或2，1为HRRN，2为RR\n";
    return 0;
}

/*
5
0 1 2 3 4
4 3 5 2 4
1

5
0 1 2 3 4
4 3 5 2 4
2
1

5
0 1 2 3 4
4 3 5 2 4
2
4
*/