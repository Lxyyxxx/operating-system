#include <bits/stdc++.h>
using namespace std;

// 磁道个数n 开始磁道号m
int m, n;

const int MaxNumber = 100;
// 磁道访问序列
int TrackOrder[MaxNumber];
// 磁头移动距离
int MoveDistance[MaxNumber];
// 平均寻道长度
double AverageDistance;
// 磁头移动方向
bool direction;
// 访问序列结果
int result[MaxNumber];

/* 先来先服务 */
void FCFS();
/* 最短寻道时间优先 */
void SSTF();
/* 扫描 */
void SCAN();
/* 循环扫描 */
void CSCAN();
/* 打印每次访问的磁道号和磁头移动距离 */
void print();

/* 打印每次访问的磁道号和磁头移动距离 */
void print() {
    cout << "每次访问磁道号";
    for (int i = 0; i < n; i++) cout << '\t' << result[i];
    cout << '\n';
    cout << "磁头移动距离";
    for (int i = 0; i < n; i++) cout << '\t' << MoveDistance[i];
    cout << '\n';
}

/* 先来先服务 */
void FCFS() {
    AverageDistance = 0.0;
    // 按顺序访问
    for (int i = 0; i < n; i++) {
        result[i] = TrackOrder[i];
        MoveDistance[i] = abs(m - TrackOrder[i]);
        m = TrackOrder[i];
        AverageDistance += MoveDistance[i];
    }
    AverageDistance /= n;
    print();
    cout << "平均寻道长度为：" << AverageDistance << '\n';
}

/* 最短寻道时间优先 */
void SSTF() {
    AverageDistance = 0.0;
    // 寻找与当前磁头所在磁道距离最近的磁道
    bool vis[MaxNumber] = {false};
    int mn_dis, mn_id;
    for (int i = 0; i < n; i++) {
        // 寻找
        mn_dis = 1e5;
        for (int j = 0; j < n; j++) {
            if (!vis[j] && abs(m - TrackOrder[j]) < mn_dis)
                mn_dis = abs(m - TrackOrder[j]), mn_id = j;
        }
        // 访问
        result[i] = TrackOrder[mn_id];
        MoveDistance[i] = mn_dis;
        m = TrackOrder[mn_id];
        AverageDistance += MoveDistance[i];
        vis[mn_id] = true;
    }
    AverageDistance /= n;
    print();
    cout << "平均寻道长度为：" << AverageDistance << '\n';
}

/* 扫描 */
void SCAN() {
    AverageDistance = 0.0;
    // 在磁头当前移动方向上寻找与当前磁头所在磁道距离最近的磁道
    pair<int, int> dis[MaxNumber];
    // 递增方向
    if (direction == 0) {
        for (int i = 0; i < n; i++) dis[i] = {TrackOrder[i], TrackOrder[i] - m};
    }
    // 递减方向
    else if (direction == 1) {
        for (int i = 0; i < n; i++) dis[i] = {TrackOrder[i], m - TrackOrder[i]};
    }
    sort(dis, dis + n, [&](pair<int, int> x, pair<int, int> y) {
        // 正的排在负的前面
        if (x.second * y.second < 0) return x.second > y.second;
        // 都是负的，大的排在前面，绝对值小
        if (x.second < 0) return x.second > y.second;
        // 距离小的排在前面
        return x.second < y.second;
    });
    // 访问
    for (int i = 0; i < n; i++) {
        result[i] = dis[i].first;
        MoveDistance[i] = abs(m - dis[i].first);
        m = dis[i].first;
        AverageDistance += MoveDistance[i];
    }
    AverageDistance /= n;
    print();
    cout << "平均寻道长度为：" << AverageDistance << '\n';
}

/* 循环扫描 */
void CSCAN() {
    AverageDistance = 0.0;
    // 在磁头当前移动方向上寻找与当前磁头所在磁道距离最近的磁道，磁头单向移动
    pair<int, int> dis[MaxNumber];
    // 递增方向
    if (direction == 0) {
        for (int i = 0; i < n; i++) dis[i] = {TrackOrder[i], TrackOrder[i] - m};
    }
    // 递减方向
    else if (direction == 1) {
        for (int i = 0; i < n; i++) dis[i] = {TrackOrder[i], m - TrackOrder[i]};
    }
    sort(dis, dis + n, [&](pair<int, int> x, pair<int, int> y) {
        // 正的排在负的前面
        if (x.second * y.second < 0) return x.second > y.second;
        // 对于正的，距离小的排在前面；对于负的，从距离最大的开始单向移动
        // 综上，值小的排在前面
        return x.second < y.second;
    });
    // 访问
    for (int i = 0; i < n; i++) {
        result[i] = dis[i].first;
        MoveDistance[i] = abs(m - dis[i].first);
        m = dis[i].first;
        AverageDistance += MoveDistance[i];
    }
    AverageDistance /= n;
    print();
    cout << "平均寻道长度为：" << AverageDistance << '\n';
}

int main() {
    cout << "请输入磁道个数：";
    cin >> n;
    cout << "请输入磁道访问序列：";
    for (int i = 0; i < n; i++) cin >> TrackOrder[i];
    cout << "请输入开始磁道号：";
    cin >> m;
    cout << "请选择算法 1-FCFS，2-SSTF，3-SCAN，4-循环SCAN：";
    int choose;
    cin >> choose;
    if (choose == 1) {
        FCFS();
    } else if (choose == 2) {
        SSTF();
    } else if (choose == 3) {
        cout << "请输入磁头移动方向 0-递增，1-递减：";
        cin >> direction;
        SCAN();
    } else if (choose == 4) {
        cout << "请输入磁头移动方向 0-递增，1-递减：";
        cin >> direction;
        CSCAN();
    }
    return 0;
}

/*
9
55 58 39 18 90 160 150 38 184
100
1

9
55 58 39 18 90 160 150 38 184
100
2

9
55 58 39 18 90 160 150 38 184
100
3
0

9
55 58 39 18 90 160 150 38 184
100
3
1

9
55 58 39 18 90 160 150 38 184
100
4
0

9
55 58 39 18 90 160 150 38 184
100
4
1
 */