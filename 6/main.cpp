#include <bits/stdc++.h>
using namespace std;

/* 物理块数 */
const int num_memory = 4;
/* 进程的页面数 */
const int num_page = 7;
/* 页面大小 */
const int size_page = 1024;
/* 物理块大小 */
const int size_memory = 1024;
/* 进程访问的逻辑地址数 */
const int num_vis = 10;

/* 页面 */
struct Page {
    int memory;
    bool P;
} page[num_page];

/* 内存 */
struct Memory {
    int page;
    bool A, M;
    int next;
} memory[num_memory];

/* 替换指针、空闲指针 */
vector<int> replace_queue, free_queue;
int cur_replace, cur_free;

/* 进程访问与操作 */
int vis[num_vis];
bool rw[num_vis];  // 0-r,1-w

/* 初始化 */
void init() {
    // 进程的页表
    page[0] = {3, 1};
    page[1] = {-1, 0};
    page[2] = {1, 1};
    page[3] = {-1, 0};
    page[4] = {0, 1};
    page[5] = {-1, 0};
    page[6] = {-1, 0};

    // 内存情况
    memory[0] = {4, 1, 1, 1};
    memory[1] = {2, 0, 1, 3};
    memory[2] = {-1, 0, 0, 2};
    memory[3] = {0, 1, 0, 0};

    // 进程访问
    vis[0] = 455, rw[0] = 0;
    vis[1] = 1029, rw[1] = 0;
    vis[2] = 2093, rw[2] = 1;
    vis[3] = 2612, rw[3] = 1;
    vis[4] = 3106, rw[4] = 0;
    vis[5] = 4149, rw[5] = 1;
    vis[6] = 5122, rw[6] = 0;
    vis[7] = 6211, rw[7] = 0;
    vis[8] = 6897, rw[8] = 0;
    vis[9] = 875, rw[9] = 1;

    // 初始化已用块循环队列和空闲块循环队列
    for (int i = 0; i < num_memory; i++) {
        if (memory[i].page != -1)
            replace_queue.push_back(i);
        else
            free_queue.push_back(i);
    }
    cur_replace = (replace_queue.empty() ? -1 : 0);
    cur_free = (free_queue.empty() ? -1 : 0);
}

/* 打印页表 */
void print_page() {
    cout << "页表：\n页号 物理块号 存在位P\n";
    for (int i = 0; i < num_page; i++) {
        cout << i << "\t";
        if (page[i].memory == -1)
            cout << "-";
        else
            cout << page[i].memory;
        cout << "\t" << page[i].P << "\n";
    }
}

/* 打印内存情况 */
void print_memory(int choose) {
    if (choose == 2)
        cout << "内存：\n物理块号 页号 访问位A 修改位M 指针（指向下一物理块号）"
                "\n";
    else if (choose == 1)
        cout << "内存：\n物理块号 页号 访问位A 指针（指向下一物理块号）"
                "\n";
    for (int i = 0; i < num_memory; i++) {
        cout << i << "\t";
        if (memory[i].page == -1)
            if (choose == 2)
                cout << "-\t-\t-";
            else
                cout << "-\t-";
        else if (choose == 2)
            cout << memory[i].page << "\t" << memory[i].A << "\t"
                 << memory[i].M;
        else if (choose == 1)
            cout << memory[i].page << "\t" << memory[i].A;
        cout << "\t" << memory[i].next << '\n';
    }
}

/* 打印已用块循环队列 */
void print_replace() {
    cout << "已用块循环队列：";
    if (replace_queue.empty()) cout << "空表";
    for (auto i : replace_queue) {
        if (replace_queue[cur_replace] == i) cout << "->";
        cout << i << " ";
    }
    cout << '\n';
}

/* 打印空闲块循环队列 */
void print_free() {
    cout << "空闲块循环队列：";
    if (free_queue.empty()) cout << "空表";
    for (auto i : free_queue) cout << i << " ";
    cout << "\n";
}

/* 逻辑地址转为物理地址 */
void address(int addr, int &page_number, int &memory_number, int &p_addr) {
    page_number = addr / size_page;
    if (page_number > size_page) {
        cout << "越界中断\n";
        page_number = -1;
        p_addr = -1;
    }
    memory_number = page[page_number].memory;
    p_addr = memory_number * size_memory + addr % size_page;
}

/* Clock置换算法 */
void Clock() {
    cout << "开始Clock算法\n";
    int page_number, memory_number, p_addr;
    for (int i = 0; i < num_vis; i++) {
        cout
            << "------------------------------------------------------------\n";
        cout << "访问：" << vis[i] << " 操作：" << (rw[i] ? "W" : "R") << "\n";
        address(vis[i], page_number, memory_number, p_addr);
        cout << "页号：" << page_number;
        // 命中
        if (page[page_number].P) {
            cout << " 物理块：" << memory_number << " 物理地址：" << p_addr
                 << "\n";
            cout << "页面命中\n";
            memory[memory_number].A = 1;
            // 未命中
        } else {
            cout << "\n页面未命中，进行页面替换\n";
            // 优先分配空闲物理块
            if (!free_queue.empty()) {
                memory_number = free_queue.front();
                memory[memory_number] = {page_number, 1, 0,
                                         memory[replace_queue.back()].next};
                page[page_number] = {memory_number, 1};
                memory[replace_queue.back()].next = memory_number;
                free_queue.erase(free_queue.begin());
                replace_queue.push_back(memory_number);
            // 无空闲物理块，进行替换
            } else {
                // 第一轮扫描，找A=0的并把访问位置0
                int num_replace = replace_queue.size();
                int replace_memory = -1;
                for (int i = cur_replace;; i = (i + 1) % num_replace) {
                    if (memory[replace_queue[i]].A == 0) {
                        replace_memory = replace_queue[i];
                        cur_replace = i + 1;
                        cur_replace %= num_replace;
                        break;
                    } else {
                        memory[replace_queue[i]].A = 0;
                    }
                    if (i == cur_replace - 1 ||
                        cur_replace == 0 && i == num_replace - 1)
                        break;
                }
                // 未找到进行二次查找
                if (replace_memory == -1) {
                    replace_memory = cur_replace;
                    cur_replace++;
                    cur_replace %= num_replace;
                }
                // 替换
                memory_number = replace_memory;
                page[page_number] = {memory_number, 1};
                page[memory[memory_number].page] = {-1, 0};
                memory[memory_number].page = page_number;
                memory[memory_number].A = 1;
            }
            // 输出物理地址
            address(vis[i], page_number, memory_number, p_addr);
            cout << "替换后：物理块：" << memory_number << " 物理地址：" << p_addr << "\n";
        }
        // 打印
        print_page();
        print_memory(1);
        print_replace();
        print_free();
    }
    cout << "Clock算法结束\n";
}

/* 改进型Clock置换算法 */
void Improve_Clock() {
    cout << "开始改进型Clock算法\n";
    int page_number, memory_number, p_addr;
    for (int i = 0; i < num_vis; i++) {
        cout
            << "------------------------------------------------------------\n";
        cout << "访问：" << vis[i] << " 操作：" << (rw[i] ? "W" : "R") << "\n";
        address(vis[i], page_number, memory_number, p_addr);
        cout << "页号：" << page_number;
        // 命中
        if (page[page_number].P) {
            cout << " 物理块：" << memory_number << " 物理地址：" << p_addr
                 << "\n";
            cout << "页面命中\n";
            memory[memory_number].A = 1;
            // 有写，修改位置1
            if (rw[i]) memory[memory_number].M = 1;
        // 未命中
        } else {
            cout << "\n页面未命中，进行页面替换\n";
            // 优先分配空闲物理块
            if (!free_queue.empty()) {
                memory_number = free_queue.front();
                memory[memory_number] = {page_number, 1, 0,
                                         memory[replace_queue.back()].next};
                page[page_number] = {memory_number, 1};
                memory[replace_queue.back()].next = memory_number;
                free_queue.erase(free_queue.begin());
                replace_queue.push_back(memory_number);
            // 无空闲物理块
            } else {
                // 第一轮找A=0,M=0的
                int num_replace = replace_queue.size();
                int replace_memory = -1;
                for (int i = cur_replace;; i = (i + 1) % num_replace) {
                    if (memory[replace_queue[i]].A == 0 &&
                        memory[replace_queue[i]].M == 0) {
                        replace_memory = replace_queue[i];
                        cur_replace = i + 1;
                        cur_replace %= num_replace;
                        break;
                    }
                    if (i == cur_replace - 1 ||
                        cur_replace == 0 && i == num_replace - 1)
                        break;
                }
                // 未找到进行二次查找,A=0,M=1,访问过的置0
                if (replace_memory == -1) {
                    for (int i = cur_replace;; i = (i + 1) % num_replace) {
                        if (memory[replace_queue[i]].A == 0 &&
                            memory[replace_queue[i]].M == 1) {
                            replace_memory = replace_queue[i];
                            cur_replace = i + 1;
                            cur_replace %= num_replace;
                            break;
                        } else {
                            memory[replace_queue[i]].A = 0;
                        }
                        if (i == cur_replace - 1 ||
                            cur_replace == 0 && i == num_replace - 1)
                            break;
                    }
                }
                // 此时A均为1，第三轮未找到找A=0，M=0
                if (replace_memory == -1) {
                    for (int i = cur_replace;; i = (i + 1) % num_replace) {
                        if (memory[replace_queue[i]].A == 0 &&
                            memory[replace_queue[i]].M == 0) {
                            replace_memory = replace_queue[i];
                            cur_replace = i + 1;
                            cur_replace %= num_replace;
                            break;
                        }
                        if (i == cur_replace - 1 ||
                            cur_replace == 0 && i == num_replace - 1)
                            break;
                    }
                }
                // 第四轮未找到找A=0，M=1
                if (replace_memory == -1) {
                    for (int i = cur_replace;; i = (i + 1) % num_replace) {
                        if (memory[replace_queue[i]].A == 0 &&
                            memory[replace_queue[i]].M == 1) {
                            replace_memory = replace_queue[i];
                            cur_replace = i + 1;
                            cur_replace %= num_replace;
                            break;
                        } else {
                            memory[replace_queue[i]].A = 0;
                        }
                        if (i == cur_replace - 1 ||
                            cur_replace == 0 && i == num_replace - 1)
                            break;
                    }
                }
                // 替换
                memory_number = replace_memory;
                page[page_number] = {memory_number, 1};
                page[memory[memory_number].page] = {-1, 0};
                memory[memory_number].page = page_number;
                memory[memory_number].A = 1;
                // 有写，修改位置1
                if (rw[i]) memory[memory_number].M = 1;
            }
            // 输出物理地址
            address(vis[i], page_number, memory_number, p_addr);
            cout << "替换后：物理块：" << memory_number << " 物理地址：" << p_addr << "\n";
        }
        // 打印
        print_page();
        print_memory(2);
        print_replace();
        print_free();
    }
    cout << "改进型Clock算法结束\n";
}

int main() {
    // 初始化
    init();
    // 打印
    print_page();
    print_memory(2);
    // 选择算法
    int choose;
    cout << "请输入置换算法，1-Clock，2-改进型Clock\n";
    cin >> choose;
    if (choose == 1) {
        Clock();
    } else if (choose == 2) {
        Improve_Clock();
    } else {
        cout << "请输入有效数字，1-Clock，2-改进型Clock\n";
    }
    return 0;
}