#include <bits/stdc++.h>
using namespace std;

// 位示图行列
const int M = 11, N = 21;
// 盘块大小
const int SIZE = 2;
// 文件数
const int FILESIZE = 100;

// 位示图
bool bitmap[M][N] = {0};
// 存相应的文件名
string bitmaps[M][N];

// 每一连续盘块的分配
struct Block {
    int start;
    int len;
};

// 文件
struct File {
    string name;
    vector<Block> block;
    int size;
};

// 文件树
struct TreeNode {
    string name;
    File info;
    vector<TreeNode *> children;
    TreeNode(string name) { this->name = name, this->info.size = 0; }
};
// 文件树的根节点
TreeNode *root = new TreeNode("/");

// 打印位示图
void print() {
    cout << " ";
    for (int i = 1; i < N; i++) cout << "\t" << i;
    cout << endl;
    for (int i = 1; i < M; i++) {
        cout << i;
        for (int j = 1; j < N; j++) {
            if (bitmap[i][j])
                cout << "\t" << bitmaps[i][j];
            else
                cout << "\t" << bitmap[i][j];
        }
        cout << endl;
    }
}

// 按'/'分割路径
vector<string> path2v(string path) {
    string aa;
    vector<string> ans;
    for (auto i : path) {
        if (i != '/')
            aa += i;
        else {
            ans.push_back(aa);
            aa = "";
        }
    }
    ans.push_back(aa);
    return ans;
}

// 创建文件
bool allocate(int size, TreeNode *t) {
    // 所占用盘块数
    int len = size / SIZE + (size % SIZE != 0), cnt = 0, cnt2 = 0;
    t->info.size += len;
    // 不断找连续块
    while (len != 0) {
        int bi = 1, bj = 1;
        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                if (!bitmap[i][j]) {
                    if (cnt == 0) {
                        bi = i;
                        bj = j;
                    }
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt == len) break;
            }
            if (cnt == len) break;
        }
        int block = N * (bi - 1) + bj;
        t->info.block.push_back({block, cnt});
        len -= cnt;
        cnt2++;
    }
    // 全找到，开始分配
    if (len == 0) {
        for (int i = 0; i < t->info.block.size(); i++) {
            int cnt1 = 0;
            int ll = t->info.block[i].len;
            int bi = (t->info.block[i].start - 1) / N + 1,
                bj = (t->info.block[i].start - 1) % N + 1;
            for (int j = bj; j < N; j++) {
                bitmap[bi][j] = 1;
                bitmaps[bi][j] = t->name;
                cnt1++;
                if (cnt1 == ll) break;
            }
            if (cnt1 < ll) {
                for (int i = bi + 1; i < M; i++) {
                    for (int j = 1; j < N; j++) {
                        bitmap[i][j] = 1;
                        bitmaps[i][j] = t->name;
                        cnt1++;
                        if (cnt1 == ll) break;
                    }
                    if (cnt1 == ll) break;
                }
            }
            cout << "分配 " << ll << " 个盘块\n";
        }
        cout << "创建文件成功，共分配 " << t->info.size << " 个盘块\n";
        return true;
    }
    // 未找到，撤销操作
    len = size / SIZE + (size % SIZE != 0);
    t->info.size -= len;
    while (cnt2--) t->info.block.pop_back();
    cout << "创建文件失败\n";
    return false;
}

// 删除文件
bool del(TreeNode *t) {
    // 删除全部连续块
    for (int i = 0; i < t->info.block.size(); i++) {
        int len = t->info.block[i].len;
        int bi = (t->info.block[i].start - 1) / N + 1,
            bj = (t->info.block[i].start - 1) % N + 1;
        // 删除
        int cnt = 0;
        for (int j = bj; j < N; j++) {
            bitmap[bi][j] = 0;
            cnt++;
            if (cnt == len) break;
        }
        if (cnt < len) {
            for (int i = bi + 1; i < M; i++) {
                for (int j = 1; j < N; j++) {
                    bitmap[i][j] = 0;
                    cnt++;
                    if (cnt == len) break;
                }
                if (cnt == len) break;
            }
        }
        if (cnt != len) {
            cout << "删除文件失败\n";
            return false;
        }
    }
    cout << "删除文件成功\n";
    return true;
}

// 扩大
bool enlarge(TreeNode *t, int size) {
    // 所占用盘块数
    int len = size / SIZE + (size % SIZE != 0), cnt = 0, cnt2 = 0;
    t->info.size += len;
    int begin = t->info.block.size();
    // 找可用连续块
    while (len != 0) {
        int bi = 1, bj = 1;
        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                if (!bitmap[i][j]) {
                    if (cnt == 0) {
                        bi = i;
                        bj = j;
                    }
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt == len) break;
            }
            if (cnt == len) break;
        }
        int block = N * (bi - 1) + bj;
        t->info.block.push_back({block, cnt});
        len -= cnt;
        cnt2++;
    }
    // 全找到，开始分配
    if (len == 0) {
        for (int i = begin; i < t->info.block.size(); i++) {
            int cnt1 = 0;
            int ll = t->info.block[i].len;
            int bi = (t->info.block[i].start - 1) / N + 1,
                bj = (t->info.block[i].start - 1) % N + 1;
            for (int j = bj; j < N; j++) {
                bitmap[bi][j] = 1;
                bitmaps[bi][j] = t->name;
                cnt1++;
                if (cnt1 == ll) break;
            }
            if (cnt1 < ll) {
                for (int i = bi + 1; i < M; i++) {
                    for (int j = 1; j < N; j++) {
                        bitmap[i][j] = 1;
                        bitmaps[i][j] = t->name;
                        cnt1++;
                        if (cnt1 == ll) break;
                    }
                    if (cnt1 == ll) break;
                }
            }
            cout << "分配 " << ll << " 个盘块\n";
        }
        cout << "扩大文件成功，共分配 " << t->info.size << " 个盘块\n";
        return true;
    }
    // 未找到，撤销
    len = size / SIZE + (size % SIZE != 0);
    t->info.size -= len;
    while (cnt2--) t->info.block.pop_back();
    cout << "扩大文件失败\n";
    return false;
}

// 缩小
bool reduce(TreeNode *t, int size) {
    int len = size / SIZE + (size % SIZE != 0);
    if (len > t->info.size) {
        cout << "缩小文件大于总文件大小，缩小失败\n";
        return false;
    }
    // 开始缩小
    t->info.size -= len;
    for (int i = 0; i < t->info.block.size(); i++) {
        int bi = (t->info.block[i].start - 1) / N + 1,
            bj = (t->info.block[i].start - 1) % N + 1;
        // 删除
        int cnt = 0;
        for (int j = bj; j < N; j++) {
            bitmap[bi][j] = 0;
            cnt++;
            if (cnt == len) break;
        }
        if (cnt < len) {
            for (int i = bi + 1; i < M; i++) {
                for (int j = 1; j < N; j++) {
                    bitmap[i][j] = 0;
                    cnt++;
                    if (cnt == len) {
                        bi = i + (j == N - 1);
                        bj = j == N - 1 ? 0 : j + 1;
                        break;
                    }
                }
                if (cnt == len) break;
            }
        }
        int block = N * (bi - 1) + bj;
        int ll = t->info.size;
        // 删除整个连续块
        if (cnt == ll) {
            t->info.block.erase(t->info.block.begin() + i);
            i--;
        }
        // 修改连续块的开始
        else {
            t->info.block[i] = {block, ll - cnt};
        }
        if (cnt == len) break;
        len -= cnt;
    }
    cout << "缩小文件成功，共分配 " << t->info.size << " 个盘块\n";
    return true;
}

// 复制文件 t1到t2
bool dup(TreeNode *t1, TreeNode *t2) {
    int len = t1->info.size, cnt = 0, cnt2 = 0;
    t2->name = t2->info.name = t1->name;
    t2->info.size = t1->info.size;
    // 找可分配连续块
    while (len != 0) {
        int bi = 1, bj = 1;
        bool f = false;
        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                if (!bitmap[i][j]) {
                    if (cnt == 0) {
                        bi = i;
                        bj = j;
                    }
                    cnt++;
                } else if (cnt > 0) {
                    f = true;
                    break;
                }
                if (cnt == len) break;
            }
            if (cnt == len || f) break;
        }
        int block = N * (bi - 1) + bj;
        t2->info.block.push_back({block, cnt});
        len -= cnt;
        cnt = 0;
        cnt2++;
    }
    // 全找到，开始分配
    if (len == 0) {
        for (int i = 0; i < t2->info.block.size(); i++) {
            int cnt1 = 0;
            int ll = t2->info.block[i].len;
            int bi = (t2->info.block[i].start - 1) / N + 1,
                bj = (t2->info.block[i].start - 1) % N + 1;
            for (int j = bj; j < N; j++) {
                bitmap[bi][j] = 1;
                bitmaps[bi][j] = t2->name;
                cnt1++;
                if (cnt1 == ll) break;
            }
            if (cnt1 < ll) {
                for (int i = bi + 1; i < M; i++) {
                    for (int j = 1; j < N; j++) {
                        bitmap[i][j] = 1;
                        bitmaps[i][j] = t2->name;
                        cnt1++;
                        if (cnt1 == ll) break;
                    }
                    if (cnt1 == ll) break;
                }
            }
            cout << "分配 " << ll << " 个盘块\n";
        }
        cout << "复制文件成功，共分配 " << t2->info.size << " 个盘块\n";
        return true;
    }
    // 未找到，撤销
    while (cnt2--) t2->info.block.pop_back();
    cout << "复制文件失败\n";
    return false;
}

// 移动t1到t2
bool cd(TreeNode *t1, TreeNode *t2) {
    t2 = t1;
    cout << "移动文件成功\n";
    return true;
}

// 创建文件
bool allocateFile(string path, int size) {
    cout << "创建文件 " << path << " 大小 " << size << " KB\n";
    TreeNode *t = root;
    vector<string> v = path2v(path);
    bool found = 0;
    // 找文件夹
    for (int i = 0; i < v.size() - 1; i++) {
        found = 0;
        for (auto j : t->children) {
            if (j->name == v[i]) {
                t = j;
                found = 1;
                break;
            }
        }
        // 没有找到则新建文件夹
        if (!found) {
            TreeNode *tmp = new TreeNode(v[i]);
            t->children.push_back(tmp);
            t = tmp;
        }
    }
    found = 0;
    // 找文件
    for (auto j : t->children) {
        if (j->name == v.back()) {
            t = j;
            found = 1;
            break;
        }
    }
    // 未找到，新建
    if (!found) {
        TreeNode *tmp = new TreeNode(v.back());
        t->children.push_back(tmp);
        t = tmp;
    } else {
        cout << "文件已存在，创建文件失败\n";
        return false;
    }
    // 开始创建
    bool f = allocate(size, t);
    // 未创建成功则删除
    if (!f) delete t;
    print();
    return f;
}

// 删除文件
bool delFile(string path) {
    cout << "删除文件 " << path << " \n";
    TreeNode *t = root;
    vector<string> v = path2v(path);
    bool found = 0;
    // 找文件夹
    for (int i = 0; i < v.size() - 1; i++) {
        found = 0;
        for (auto j : t->children) {
            if (j->name == v[i]) {
                t = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            cout << "找不到文件夹 " << v[i] << " 删除文件失败\n";
            return false;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd = nullptr;
    for (int i = 0; i < t->children.size(); i++) {
        if (t->children[i]->name == v.back()) {
            fd = t->children[i];
            found = 1;
            // 从文件夹中删除文件
            t->children.erase(t->children.begin() + i);
            break;
        }
    }
    // 未找到
    if (!found) {
        cout << "找不到文件夹 " << v.back() << " 删除文件失败\n";
        return false;
    }
    // 开始删除
    bool f = del(fd);
    // 删除成功才把指针删除
    if (f)
        // 删除文件
        delete fd;
    print();
    return f;
}

// 扩大文件空间
bool enlargeFile(string path, int size) {
    cout << "扩大文件 " << path << " 大小 " << size << " KB\n";
    TreeNode *t = root;
    vector<string> v = path2v(path);
    bool found = 0;
    // 找文件夹
    for (int i = 0; i < v.size() - 1; i++) {
        found = 0;
        for (auto j : t->children) {
            if (j->name == v[i]) {
                t = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            cout << "找不到文件夹 " << v[i] << " 扩大文件失败\n";
            return false;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd = nullptr;
    for (int i = 0; i < t->children.size(); i++) {
        if (t->children[i]->name == v.back()) {
            fd = t->children[i];
            found = 1;
            break;
        }
    }
    // 未找到
    if (!found) {
        cout << "找不到文件夹 " << v.back() << " 扩大文件失败\n";
        return false;
    }
    // 开始扩大
    bool f = enlarge(fd, size);
    print();
    return f;
}

// 缩小文件空间
bool reduceFile(string path, int size) {
    cout << "缩小文件 " << path << " 大小 " << size << " KB\n";
    TreeNode *t = root;
    vector<string> v = path2v(path);
    bool found = 0;
    // 找文件夹
    for (int i = 0; i < v.size() - 1; i++) {
        found = 0;
        for (auto j : t->children) {
            if (j->name == v[i]) {
                t = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            cout << "找不到文件夹 " << v[i] << " 缩小文件失败\n";
            return false;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd = nullptr;
    for (int i = 0; i < t->children.size(); i++) {
        if (t->children[i]->name == v.back()) {
            fd = t->children[i];
            found = 1;
            break;
        }
    }
    // 未找到
    if (!found) {
        cout << "找不到文件夹 " << v.back() << " 缩小文件失败\n";
        return false;
    }
    // 开始缩小
    bool f = reduce(fd, size);
    print();
    return f;
}

// 复制文件
bool dupFile(string path1, string path2) {
    cout << "复制文件 " << path1 << " 至 " << path2 << endl;
    TreeNode *t1 = root, *t2 = root;
    vector<string> v1 = path2v(path1), v2 = path2v(path2);
    bool found = 0;
    // 找文件夹
    for (int i = 0; i < v1.size() - 1; i++) {
        found = 0;
        for (auto j : t1->children) {
            if (j->name == v1[i]) {
                t1 = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            cout << "找不到文件夹 " << v1[i] << " 复制文件失败\n";
            return false;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd1 = nullptr;
    for (int i = 0; i < t1->children.size(); i++) {
        if (t1->children[i]->name == v1.back()) {
            fd1 = t1->children[i];
            found = 1;
            break;
        }
    }
    // 未找到
    if (!found) {
        cout << "找不到文件夹 " << v1.back() << " 复制文件失败\n";
        return false;
    }
    // file2
    found = 0;
    // 找文件夹
    for (int i = 0; i < v2.size() - 1; i++) {
        found = 0;
        for (auto j : t2->children) {
            if (j->name == v2[i]) {
                t2 = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            TreeNode *tmp = new TreeNode(v2[i]);
            t2->children.push_back(tmp);
            t2 = tmp;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd2 = nullptr;
    for (int i = 0; i < t2->children.size(); i++) {
        if (t2->children[i]->name == v2.back()) {
            fd2 = t2->children[i];
            found = 1;
            break;
        }
    }
    // 未找到
    if (!found) {
        fd2 = new TreeNode(v2.back());
        t2->children.push_back(fd2);
    } else {
        cout << "文件已存在，复制文件失败\n";
        return false;
    }
    // 复制
    bool f = dup(fd1, fd2);
    // 复制失败删除文件
    if (!f) delete fd2;
    print();
    return f;
}

// 移动文件
bool cdFile(string path1, string path2) {
    cout << "移动文件 " << path1 << " 至 " << path2 << endl;
    TreeNode *t1 = root, *t2 = root;
    vector<string> v1 = path2v(path1), v2 = path2v(path2);
    bool found = 0;
    // 找文件夹
    for (int i = 0; i < v1.size() - 1; i++) {
        found = 0;
        for (auto j : t1->children) {
            if (j->name == v1[i]) {
                t1 = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            cout << "找不到文件夹 " << v1[i] << " 移动文件失败\n";
            return false;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd1 = nullptr;
    for (int i = 0; i < t1->children.size(); i++) {
        if (t1->children[i]->name == v1.back()) {
            fd1 = t1->children[i];
            found = 1;
            break;
        }
    }
    // 未找到
    if (!found) {
        cout << "找不到文件夹 " << v1.back() << " 移动文件失败\n";
        return false;
    }
    // file2
    found = 0;
    // 找文件夹
    for (int i = 0; i < v2.size() - 1; i++) {
        found = 0;
        for (auto j : t2->children) {
            if (j->name == v2[i]) {
                t2 = j;
                found = 1;
                break;
            }
        }
        // 没有找到
        if (!found) {
            TreeNode *tmp = new TreeNode(v2[i]);
            t2->children.push_back(tmp);
            t2 = tmp;
        }
    }
    found = 0;
    // 找文件
    TreeNode *fd2 = nullptr;
    for (int i = 0; i < t2->children.size(); i++) {
        if (t2->children[i]->name == v2.back()) {
            fd2 = t2->children[i];
            found = 1;
            break;
        }
    }
    // 未找到
    if (!found) {
        fd2 = new TreeNode(v2.back());
        t2->children.push_back(fd2);
        t2 = fd2;
    } else {
        cout << "文件已存在，移动文件失败\n";
        return false;
    }
    // 开始移动
    bool f = cd(fd1, fd2);
    // 移动成功，删除源文件
    if (f) {
        // 删除fd1
        for (int i = 0; i < t1->children.size(); i++) {
            if (t1->children[i]->name == v1.back()) {
                t1->children.erase(t1->children.begin() + i);
                break;
            }
        }
        delete fd1;
    }
    // 否则，移动失败，删除目标文件
    else
        delete fd2;
    print();
    return f;
}

int main() {
    // 创建文件
    allocateFile("d1/d2/d3/f1", 50);
    allocateFile("f2", 200);
    allocateFile("f3", 100);
    // 删除文件
    delFile("d1/d2/d3/f1");
    delFile("f3");
    // 创建文件
    allocateFile("f4", 120);
    // 扩大文件空间
    enlargeFile("f4", 80);
    // 缩小文件空间
    reduceFile("f4", 120);
    // 复制文件
    dupFile("f4", "d1/d2/d3/f4");
    // 移动文件
    cdFile("d1/d2/d3/f4", "d1/d2/d3/d4/f4");
    return 0;
}