#include <bits/stdc++.h>
using namespace std;

// 位示图行列
const int M = 11, N = 21;
// 盘块大小
const int SIZE = 2;
// 文件数
const int FILESIZE = 100;

// 位示图
bool bitmap[M][N] = {0};
// 文件及对应结构体数组下标
unordered_map<string, int> file_mp;

// 文件
struct File
{
    string name;
    int start;
    int len;
} file[FILESIZE];
int file_cur = 0;

bool allocate(string name, int size)
{
    cout << "创建文件 " << name << " 大小 " << size << " KB\n";
    // 所占用盘块数
    int len = size / SIZE + (size % SIZE != 0), cnt = 0;
    int bi = 0, bj = 0;
    for (int i = 1; i < M; i++)
    {
        for (int j = 1; j < N; j++)
        {
            if (!bitmap[i][j])
            {
                if (cnt == 0)
                {
                    bi = i;
                    bj = j;
                }
                cnt++;
            }
            else
            {
                cnt = 0;
            }
            if (cnt == len)
                break;
        }
        if (cnt == len)
            break;
    }
    // 找到，开始分配
    if (cnt == len)
    {
        int block = N * (bi - 1) + bj;
        file[file_cur] = {name, block, len};
        int cnt1 = 0;
        for (int j = bj; j < N; j++)
        {
            bitmap[bi][j] = 1;
            cnt1++;
            if (cnt1 == len)
                break;
        }
        if (cnt1 < len)
        {
            for (int i = bi + 1; i < M; i++)
            {
                for (int j = 1; j < N; j++)
                {
                    bitmap[i][j] = 1;
                    cnt1++;
                    if (cnt1 == len)
                        break;
                }
                if (cnt1 == len)
                    break;
            }
        }
        file_mp[name] = file_cur++;
        cout << "创建文件成功，共分配 " << len << " 个盘块\n";
        return true;
    }
    cout << "创建文件失败\n";
    return false;
}

bool del(string name)
{
    cout << "删除文件 " << name << " \n";
    if (file_mp.count(name) == 0)
    {
        cout << "删除文件失败，文件不存在\n";
        return false;
    }
    int id = file_mp[name];
    int bi = (file[id].start - 1) / N + 1, bj = (file[id].start - 1) % N + 1;
    // 删除
    int cnt = 0;
    for (int j = bj; j < N; j++)
    {
        bitmap[bi][j] = 0;
        cnt++;
        if (cnt == file[id].len)
            break;
    }
    if (cnt < file[id].len)
    {
        for (int i = bi + 1; i < M; i++)
        {
            for (int j = 1; j < N; j++)
            {
                bitmap[i][j] = 0;
                cnt++;
                if (cnt == file[id].len)
                    break;
            }
            if (cnt == file[id].len)
                break;
        }
    }
    if (cnt == file[id].len)
    {
        file_mp.erase(name);
        cout << "删除文件成功\n";
        return true;
    }
    cout << "删除文件失败\n";
    return false;
}

void print()
{
    cout << " ";
    for (int i = 1; i < N; i++)
        cout << "\t" << i;
    cout << endl;
    for (int i = 1; i < M; i++)
    {
        cout << i;
        for (int j = 1; j < N; j++)
        {
            cout << "\t" << bitmap[i][j];
        }
        cout << endl;
    }
}

int main()
{
    // 创建文件
    allocate("f1", 50);
    print();
    allocate("f2", 200);
    print();
    allocate("f3", 100);
    print();
    // 删除文件
    del("f1");
    print();
    del("f2");
    print();
    // 创建文件
    allocate("f4", 120);
    print();
    return 0;
}