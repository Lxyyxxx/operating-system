#include <bits/stdc++.h>
using namespace std;

const int MaxNumber = 100;
// 可利用资源向量
int Available[MaxNumber];
// 最大需求矩阵
int Max[MaxNumber][MaxNumber];
// 分配矩阵
int Allocation[MaxNumber][MaxNumber];
// 需求矩阵
int Need[MaxNumber][MaxNumber];
// 请求资源
int Request[MaxNumber];
// 安全序列
int SafeOrder[MaxNumber];
// 工作向量
int Work[MaxNumber];
// 完成向量
bool Finish[MaxNumber];
// 进程数，资源数，请求资源的进程
int n, m, pk;

// 安全性检查
bool safe_check();
// 找到一个符合条件的进程
int find_one();

// 银行家算法
int banker() {
    for (int j = 0; j < m; j++) {
        if (Request[j] > Need[pk][j]) {
            return -1;
        }
    }
    // 判断请求的是否超过需求量
    for (int j = 0; j < m; j++) {
        if (Request[j] > Available[j]) {
            return -2;
        }
    }
    // 试探分配
    for (int j = 0; j < m; j++) {
        Available[j] -= Request[j];
        Allocation[pk][j] += Request[j];
        Need[pk][j] -= Request[j];
    }
    bool save = safe_check();
    if (!save) {
        // 恢复
        for (int j = 0; j < m; j++) {
            Available[j] += Request[j];
            Allocation[pk][j] -= Request[j];
            Need[pk][j] += Request[j];
        }
        return -3;
    }
    return 0;
}

// 安全性检查
bool safe_check() {
    int cur = 0;
    for (int i = 0; i < m; i++) Work[i] = Available[i];
    fill(Finish, Finish + n, false);
    int id = find_one();
    while (id < n) {
        for (int j = 0; j < m; j++) Work[j] += Allocation[id][j];
        Finish[id] = true;
        // 加入安全序列
        SafeOrder[cur++] = id;
        // 继续找下一个
        id = find_one();
    }
    // 检查是否都能成功完成
    bool f = true;
    for (int i = 0; i < n; i++) {
        if (!Finish[i]) {
            f = false;
            break;
        }
    }
    return f;
}

// 找到一个符合条件的进程
int find_one() {
    int id;
    bool f;
    for (id = 0; id < n; id++) {
        if (!Finish[id]) {
            f = true;
            for (int j = 0; j < m; j++)
                if (Need[id][j] > Work[j]) {
                    f = false;
                    break;
                }
            if (f) break;
        }
    }
    return id;
}

// 输出安全序列
void print() {
    cout << "安全序列：\n";
    // 安全序列
    for (int i = 0; i < n; i++) {
        cout << SafeOrder[i] << " ";
        cout << "Max：";
        for (int j = 0; j < m; j++) cout << Max[SafeOrder[i]][j] << " ";
        cout << "Allocation：";
        for (int j = 0; j < m; j++) cout << Allocation[SafeOrder[i]][j] << " ";
        cout << "Need：";
        for (int j = 0; j < m; j++) cout << Need[SafeOrder[i]][j] << " ";
        cout << "Avaliable：";
        for (int j = 0; j < m; j++) cout << Available[j] << " ";
        cout << endl;
    }
}

int main() {
    // 用户输入初始化
    cout << "进程数：";
    cin >> n;
    cout << "资源数：";
    cin >> m;
    cout << "Max：\n";
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) cin >> Max[i][j];
    cout << "Allocation：\n";
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) cin >> Allocation[i][j];
    cout << "Need：\n";
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) cin >> Need[i][j];
    cout << "Available：\n";
    for (int j = 0; j < m; j++) cin >> Available[j];
    // 判断当前状态安全与否，如果不安全则结束，否则输出安全序列并继续资源请求
    bool safe = safe_check();
    if (!safe) {
        cout << "不安全\n";
        return 0;
    } else
        print();
    // 是否需要请求新的
    bool re;
    cout << "是否需要请求资源(0-否,1-是)：";
    cin >> re;
    while (re) {
        cout << "请求进程：";
        cin >> pk;
        cout << "Request：";
        for (int j = 0; j < m; j++) cin >> Request[j];
        // 银行家算法
        int state = banker();
        if (state == -1)
            cout << "请求失败，请求量大于需求量\n";
        else if (state == -2)
            cout << "请求失败，请求量大于可分配数量\n";
        else if (state == -3)
            cout << "请求失败，系统不安全\n";
        else if (state == 0)
            print();
        cout << "是否需要请求资源(0-否,1-是)：";
        cin >> re;
    }
    return 0;
}

/*
5 3
7	5	3
3	2	2
9	0	2
2	2	2
4	3	3
0	1	0
2	0	0
3	0	2
2	1	1
0	0	2
7	4	3
1	2	2
6	0	0
0	1	1
4	3	1
3	3	2
1
1
1 0 2
1
4
3 3 0
1
0
0 2 0
1
0
0 1 0
0
*/