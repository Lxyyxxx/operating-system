#include <bits/stdc++.h>
using namespace std;

// 进程调度时间变量
const int MaxNum = 100;
// 到达时间
int ArrivalTime[MaxNum];
// 服务时间
int ServiceTime[MaxNum];
// 完成时间
int FinishTime[MaxNum];
// 周转时间
int WholeTime[MaxNum];
// 带权周转时间
double WeightWholeTime[MaxNum];
// 周转时间的平均值
double AverageWT_FCFS, AverageWT_SJF;
// 带权周转时间的平均值
double AverageWWT_FCFS, AverageWWT_SJF;

// 先来先服务
// n-进程个数
void FCFS(int n) {
    cout << "FCFS开始调度\n";
    // 当前时刻，表示哪个进程开始运行了
    int current = ArrivalTime[0];
    // 按到达时间运行，那就是直接到达时间数组遍历即可
    for (int i = 0; i < n; i++) {
        cout << "时刻" << current << "：进程" << char(i + 'A') << "开始运行\n";
        // 完成=开始服务+服务
        FinishTime[i] = current + ServiceTime[i];
        // 周转=完成-到达
        WholeTime[i] = FinishTime[i] - ArrivalTime[i];
        // 带权周转=周转/服务
        WeightWholeTime[i] = WholeTime[i] * 1.0 / ServiceTime[i];
        cout << "时刻" << FinishTime[i] << "：进程" << char(i + 'A')
             << "运行完成";
        cout << "\n周转时间：" << WholeTime[i];
        cout << "\n带权周转时间：" << WeightWholeTime[i] << '\n';
        // 当前时刻设为第i个进程已完成时间，作为下一个进程的开始执行时间
        current = FinishTime[i];
    }
    cout << "时刻" << current << "：所有进程运行完毕\n";
    // 计算周转时间的平均值和带权周转时间的平均值
    double Sum_WT_FCFS = 0, Sum_WWT_FCFS = 0;
    for (int i = 0; i < n; i++) {
        Sum_WT_FCFS += WholeTime[i];
        Sum_WWT_FCFS += WeightWholeTime[i];
    }
    Sum_WT_FCFS /= n, Sum_WWT_FCFS /= n;
    cout << "平均周转时间：" << Sum_WT_FCFS << "\n平均带权周转时间："
         << Sum_WWT_FCFS;
    cout << "\nFCFS调度结束\n";
}

// 短作业优先
// n-进程个数
void SJF(int n) {
    cout << "SJF开始调度\n";
    // 当前时刻，表示哪个进程开始运行了
    // 开始时间为第一个到达时间
    int current = ArrivalTime[0];
    // 记录是否运行过
    bool vis[MaxNum] = {false};
    // 进程编号，最短作业时间，最短作业即选择执行的进程
    int cur, fast, fast_cur;
    // 当前未执行的进程个数
    int wait = n;
    // 每执行一个进程都做一次操作
    while (wait--) {
        // 从第一个开始找，找已到达且未执行且作业最短的进程
        cur = 0;
        fast = INT_MAX;
        while (cur >= 0 && cur < n && ArrivalTime[cur] <= current) {
            if (!vis[cur] && ServiceTime[cur] < fast) {
                fast = ServiceTime[cur];
                fast_cur = cur;
            }
            cur++;
        }
        // 执行它
        vis[fast_cur] = true;
        cout << "时刻" << current << "：进程" << char(fast_cur + 'A')
             << "开始运行\n";
        // 完成=开始服务+服务
        FinishTime[fast_cur] = current + ServiceTime[fast_cur];
        // 周转=完成-到达
        WholeTime[fast_cur] = FinishTime[fast_cur] - ArrivalTime[fast_cur];
        // 带权周转=周转/服务
        WeightWholeTime[fast_cur] =
            WholeTime[fast_cur] * 1.0 / ServiceTime[fast_cur];
        cout << "时刻" << FinishTime[fast_cur] << "：进程"
             << char(fast_cur + 'A') << "运行完成";
        cout << "\n周转时间：" << WholeTime[fast_cur];
        cout << "\n带权周转时间：" << WeightWholeTime[fast_cur] << '\n';
        // 当前时刻设为已完成时间，作为下一个进程的开始执行时间
        current = FinishTime[fast_cur];
    }
    cout << "时刻" << current << "：所有进程运行完毕\n";
    // 计算周转时间的平均值和带权周转时间的平均值
    double Sum_WT_FCFS = 0, Sum_WWT_FCFS = 0;
    for (int i = 0; i < n; i++) {
        Sum_WT_FCFS += WholeTime[i];
        Sum_WWT_FCFS += WeightWholeTime[i];
    }
    Sum_WT_FCFS /= n, Sum_WWT_FCFS /= n;
    cout << "平均周转时间：" << Sum_WT_FCFS << "\n平均带权周转时间："
         << Sum_WWT_FCFS;
    cout << "\nSJF调度结束\n";
}

int main() {
    // 进程个数
    int n;
    cin >> n;
    if (n > MaxNum) {
        cout << n << "超过了最大限制，最大为" << MaxNum;
        return 0;
    }
    // 选择算法，1-FCFS，2-SJF
    int choose;
    // 输入到达时间
    // 默认是按时间顺序输入的，后面无排序操作
    for (int i = 0; i < n; i++) {
        cin >> ArrivalTime[i];
    }
    // 输入服务时间
    for (int i = 0; i < n; i++) {
        cin >> ServiceTime[i];
    }
    // 输入算法选择
    cin >> choose;
    if (choose == 1)
        FCFS(n);
    else if (choose == 2)
        SJF(n);
    else
        cout << "请输入1或2，1为FCFS，2为SJF\n";
    return 0;
}

/*
input
FCFS
5
0 1 2 3 4
4 3 5 2 4
1

SJF
5
0 1 2 3 4
4 3 5 2 4
2

作业里的例子
FCFS
5
0 1 3 4 6
5 7 3 8 2
1

SJF
5
0 1 3 4 6
5 7 3 8 2
2
*/